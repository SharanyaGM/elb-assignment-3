#include <fcntl.h>
#include <unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<semaphore.h>
#include<sys/wait.h>
#include<sys/types.h>
#define MAX 4026 

int main() {
  	int fd,len, nbytes;
	int ret;
	int status;
	sem_t *sem1;
	sem1=  sem_open("sem1",O_CREAT,0666,1);
	if(sem1==SEM_FAILED)
	{
		return -1;
	}
	fd = open("/proc/proc_test/proc_sample", O_RDWR);
 	if(fd<0) {
    	perror("open");
    	exit(1);
  	}
  
	char rbuf[MAX];
	int maxlen = MAX;
	ret=fork();
	if(ret<0)
	{
		return -1;
	}
	if(ret==0)
	{

		printf("child entry ..pid=%d\n",getpid());
		sem_wait(sem1);
		printf("child exit --\n");
	
	}
	if(ret>0)
	{
		printf("inside parent--%d\n",getpid());
  		nbytes=read(fd, rbuf, maxlen);
  	if(nbytes<0) {
    	perror("read");
    	exit(3);
  	}
  	rbuf[nbytes]='\0';
  	printf("pid=%d,ppid=%d\n",getpid(),getppid());
  	printf("%s\n",rbuf);
  	sem_post(sem1);
  	waitpid(ret,&status,0);
  	}
  close(fd);
 
  return 0;
}
