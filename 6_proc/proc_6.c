#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/debugfs.h>
#include <linux/fs.h>
#include<linux/proc_fs.h>
#include<linux/sched.h>
#include<linux/sched/signal.h>
#include<linux/seq_file.h>


static struct proc_dir_entry *pdir;
static struct proc_dir_entry *pentry;


#define MAX_SIZE 4026
int len=MAX_SIZE;
char pbuffer[MAX_SIZE];


static int proc_show_demo(struct seq_file *m, void *v)
{
	struct task_struct *task_list;
	for_each_process(task_list) 
	{
	seq_printf(m, "pid: %i, ppid: %i \n",task_list->pid,task_ppid_nr(task_list));
	 }
	return 0; 
}

static int proc_open_demo(struct inode *inode, struct file *file)
{
	return single_open(file, proc_show_demo, NULL);
}
struct file_operations fops = { 
  .open=proc_open_demo,
  .release=single_release,
  .read = seq_read,
  .llseek=seq_lseek,
}; 

static int __init dfs_demo_init(void) {        //init_module
pdir=proc_mkdir("proc_test",NULL);
pentry=proc_create("proc_sample",0666,pdir,&fops);
proc_set_user(pentry,KUIDT_INIT(0),KGIDT_INIT(0));
proc_set_size(pentry,80);
      	printk("Hello World..welcome\n");
  return 0;
}
static void __exit dfs_demo_exit(void) {       //cleanup_module
  remove_proc_entry("proc_sample",pdir);
  remove_proc_entry("proc_test",NULL);
	printk("Bye,Leaving the world\n");
}
module_init(dfs_demo_init);
module_exit(dfs_demo_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sharanya");
MODULE_DESCRIPTION("A Hello, World Module");

