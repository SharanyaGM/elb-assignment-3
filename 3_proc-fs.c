#include<linux/proc_fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/kfifo.h>

static struct proc_dir_entry *pdir;
static struct proc_dir_entry *pentry;

unsigned char *pbuffer;
#define MAX_SIZE 256

struct kfifo myfifo;

int pdemo_open(struct inode* inode , struct file* file)
{
   printk("Pseudo--open method\n");
   return 0;
}
int pdemo_close(struct inode* inode , struct file* file)
{
   printk("Pseudo--release method\n");
   return 0;
}

//read file operation
static ssize_t pdemo_read(struct file *fp, char __user *ubuf, 
                                size_t count, loff_t *position) 
{ 
   int ret,ret1;
   int rcount, klen;
   void* tbuf;

   dump_stack();
   printk("Pseudo--read method\n");


   if(kfifo_is_empty(&myfifo))
   {
        printk("buffer is empty\n");
        return 0;
   }

   rcount = count;
   klen = kfifo_len(&myfifo);
   if(rcount > klen)
      rcount = klen;            
   tbuf = kmalloc(rcount, GFP_KERNEL);       
   ret1=kfifo_out(&myfifo, tbuf, rcount);
   ret=copy_to_user(ubuf, tbuf, rcount);
   if(ret)
   {
      printk("copy from user failed\n");
      return -EFAULT;
   }
   kfree(tbuf);

   printk("read operation success\n");
   return rcount;
     
} 
 
// write file operation 
static ssize_t pdemo_write(struct file *fp, const char __user *ubuf, 
                                size_t count, loff_t *position) 
{ 
   int ret;
   int kremain, wcount;
   void* tbuf;
   dump_stack();
   printk("Pseudo--write method\n");

   if(kfifo_is_full(&myfifo))       
   {
      printk("buffer is full, no space to write\n");
      return -ENOSPC;
   }

   wcount = count;
   kremain = kfifo_avail(&myfifo);
   if(kremain < count)
      wcount =  kremain ;       
   tbuf = kmalloc(wcount, GFP_KERNEL);        
   ret=copy_from_user(tbuf, ubuf, wcount);
   if(ret)
   {
      printk("copy from user failed\n");
      return -EFAULT;
   }
   kfifo_in(&myfifo, tbuf, wcount);
   kfree(tbuf);

   printk("successfully written the content"); 
   return wcount;
         
} 

static const struct file_operations fops = { 
  .read = pdemo_read, 
  .write = pdemo_write, 
  .open=pdemo_open,
  .release=pdemo_close,
}; 

 

static int __init dfs_demo_init(void) {        
pdir=proc_mkdir("procfs_test",NULL);
pentry=proc_create("procfs_sample",0666,pdir,&fops);
proc_set_user(pentry,KUIDT_INIT(0),KGIDT_INIT(0));
proc_set_size(pentry,80);
pbuffer = kmalloc(MAX_SIZE, GFP_KERNEL);
  if(pbuffer == NULL)
  {
     printk("Pseudo : kmalloc failed\n");
     return -ENOMEM;             
  }
  kfifo_init(&myfifo, pbuffer, MAX_SIZE);
      	printk("Hello World..welcome\n");
  return 0;
}
static void __exit dfs_demo_exit(void) {       
  remove_proc_entry("procfs_sample",pdir);
  remove_proc_entry("procfs_test",NULL);
  kfifo_free(&myfifo);
  printk("Bye,Leaving the world\n");
}
module_init(dfs_demo_init);
module_exit(dfs_demo_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sharanya");
MODULE_DESCRIPTION("procfs");
